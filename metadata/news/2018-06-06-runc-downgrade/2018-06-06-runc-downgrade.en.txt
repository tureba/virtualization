Title: sys-apps/runc downgrade
Author: Arnaud Lefebvre <arnaud.lefebvre@clever-cloud.com>
Content-Type: text/plain
Posted: 2018-06-06
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: sys-apps/runc

sys-apps/runc needs to be upgraded with the `--permit-downgrade "runc"` flag
because I got the previous version number wrong. This is trully an upgrade, not a downgrade.

It shouldn't happen anymore since docker is now mostly using tagged versions instead
of random commits for its dependencies.

Apologies for the noise.
