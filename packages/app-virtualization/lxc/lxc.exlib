# Copyright 2010 Alex Elsayed <eternaleye@gmail.com>
# Copyright 2010 Sterling X. Winter <replica@exherbo.org>
# Copyright 2011 Mickaël Guérin <kael@crocobox.org>
# Copyright 2014-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ tag=${PNV} ] bash-completion systemd-service autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 1.12 1.11 1.10 ] ]

export_exlib_phases src_prepare src_install

SUMMARY="Userspace tools for Linux Containers (lxc)"
DESCRIPTION="
Virtualized container technology is actively being pushed into the mainstream
Linux kernel, providing resource management through control groups (aka process
containers) and resource isolation through namespaces. Linux Containers, lxc,
aims to use these new functionalities to provide a userspace container object
which provides full resource isolation and resource control for an application
or system.
"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    busybox  [[ description = [ Support for creating BusyBox-based containers ] ]]
    debian   [[ description = [ Support for creating Debian and Ubuntu containers ] ]]
    doc      [[ description = [ Install API documentation using doxygen ] ]]
    examples [[ description = [ Install example configuration files ] ]]
    fedora   [[ description = [ Support for creating Fedora containers ] ]]
    libvirt  [[ description = [ Use libvirt for networking ] ]]
"

DEPENDENCIES="
    build:
        sys-kernel/linux-headers
        doc? (
            app-doc/doxygen
            app-text/docbook-utils
        )
    build+run:
        sys-libs/libcap
        sys-libs/libseccomp
        !app-doc/docbook2X [[
            description = [ app-doc/docbook2X can break the build. Uninstall it. ]
            resolution = uninstall-blocked-before
        ]]
    run:
        dev-libs/gnutls
        busybox? ( sys-apps/busybox )
        debian?  ( dev-util/debootstrap )
        fedora?  ( sys-apps/yum )
        libvirt? ( virtualization-lib/libvirt )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-capabilities
    --enable-seccomp
    --disable-apparmor
    --disable-cgmanager
    --disable-lua
    --disable-python
    --disable-selinux
    --localstatedir=/var
    --with-config-path=/etc/${PN}
    --with-distro=unknown
    --with-init-script=systemd
    --with-systemdsystemunitdir="${SYSTEMDSYSTEMUNITDIR}"
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "bash-completion bash"
    "doc"
    "doc api-docs"
    examples
)

AT_M4DIR="config"

lxc_src_prepare() {
    default

    edo sed -i -e "s:-Werror::" configure.ac
    eautoreconf
}

lxc_src_install() {
    default

    if option libvirt ; then
        insinto /usr/share/factory/etc/${PN}
        newins config/etc/default.conf.libvirt default.conf
    fi

	install_systemd_files

    edo find "${IMAGE}" -type d -empty -delete
}

