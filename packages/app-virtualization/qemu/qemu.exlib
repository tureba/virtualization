# Copyright 2009-2018 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require udev-rules

export_exlib_phases src_prepare src_configure src_test src_install pkg_postinst

SUMMARY="Generic F/OSS machine emulator and virtualizer"
DESCRIPTION="
QEMU emulates CPUs through dynamic binary translation and provides a set of device
models to run a variety of unmodified guest operating systems. QEMU can also be
used purely for CPU emulation for user level processes, allowing applications
compiled for one architecture to be run on another.
QEMU now includes full support for KVM (Kernel-based Virtual Machine) which is a
full virtualization solution for Linux on x86 hardware containing virtualization
extensions (Intel VT or AMD-V).
The kernel component of KVM is included in mainline Linux, as of 2.6.20.
"
HOMEPAGE="http://${PN}.org/"
DOWNLOADS="http://wiki.${PN}-project.org/download/${PNV}.tar.bz2"

REMOTE_IDS="freecode:qemu"

LICENCES="GPL-2"
SLOT="0"

MYOPTIONS="
    alsa
    async [[ description = [ Enable asynchronous I/O using the kernels native interface ] ]]
    bluetooth
    debug [[ description = [ Enable common debugging features ] ]]
    gnutls [[ description = [ Enable TLS encryption for VNC server ] ]]
    gtk3 [[ description = [ Enables the GTK+-3 GUI part of qemu ] ]]
    iasl [[ description = [ Enable QEMU to create ACPI tables directly ] ]]
    iscsi [[ description = [ Enable iSCSI support ] ]]
    jemalloc [[ description = [ Improve memory allocation & performance (especially IOPS) ] ]]
    jpeg [[ description = [ Enable JPEG lossy compression for VNC server ] ]]
    lzo [[ description = [ Support lzo compression ] ]]
    nfs [[ description = [ Enable built-in NFS client support ] ]]
    opengl
    pam [[ description = [ Enable PAM access control ] ]]
    png [[ description = [ Enable PNG compression for VNC server ] ]]
    pulseaudio
    quorum [[ description = [ RAID-like protection for VM images by performing multiple writes and voting on the result ] ]]
    rbd [[ description = [ Support storage on rados block devices (rbd; ceph) ] ]]
    sasl [[ description = [ Enables SASL authentication for VNC server ] ]]
    sdl2 [[ description = [ Enables the SDL:2 GUI part of qemu ] ]]
    (
      sdl-image [[ description = [ Enables the SDL Image support for icons ] ]]
    )
    smartcard [[ description = [ Enables smartcard support ] ]]
    snappy [[ description = [ Support snappy compression ] ]]
    spice [[ description = [ Enable the SPICE protocol, a remote-display system ] ]]
    ssh-block-dev [[ description = [ Enable support for ssh block devices ] ]]
    usb-passthrough [[ description = [ Enable USB passthrough support ] ]]
    usb-redirection [[ description = [ Enable USB redirection support ] ]]
    vde [[ description = [ Enable support for VDE-based networking ] ]]
    virtfs [[ description = [ Enable support for Plan 9 folder sharing over VirtIO ] requires = [ xattr ] ]]
    virtio-gpu [[ description = [ Enable VirtIO-GPU-based 3D support ] ]]
    xattr
    xfs [[ description = [ Enable xfsctl support ] ]]
    (
        aarch64 [[ description = [ build the AArch64 (ARM64) target ] ]]
        amd64 [[ description = [ build the amd64 (x86_64) target ] ]]
        arm [[ description = [ build the arm target ] ]]
        mips [[ description = [ build the MIPS (big endian) target ] ]]
        mipsel [[ description = [ build the MIPS (little endian) target ] ]]
        ppc [[ description = [ build the ppc (powerpc) target ] ]]
        ppc64 [[ description = [ build the ppc64 (powerpc64) target ] ]]
        x86 [[ description = [ build the x86 target ] ]]
    ) [[ number-selected = at-least-one ]]
    jpeg? ( ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]] )
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# Unpackaged dependencies:
# netmap? ( net-libs/netmap )
DEPENDENCIES="
    build:
        dev-lang/python:*
        sys-kernel/linux-headers[>=4.3] [[ note = [ for membarriers ] ]]
        virtual/pkg-config
        spice? ( virtualization-lib/spice-protocol[>=0.12.4] )
    build+run:
        dev-libs/glib:2[>=2.40]
        dev-libs/libxml2:2.0
        dev-libs/nettle:=[>=2.7.1]
        net-misc/curl[>=7.19.6]
        sys-apps/util-linux[>=2.16.1]
        sys-libs/libcap[>=2.24-r1] [[ description = [ needs an upstream patch ] ]]
        sys-libs/libcap-ng
        sys-libs/libseccomp[>=2.2.0]
        x11-libs/pixman:1[>=0.21.8]
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=221] )
        alsa? ( sys-sound/alsa-lib[>=1.0.13] )
        async? ( dev-libs/libaio[>=0.3.107] )
        bluetooth? ( net-wireless/bluez[>=4.9] )
        gnutls? ( dev-libs/gnutls[>=3.1.18] )
        gtk3? (
            dev-libs/vte:2.91[>=0.32.0]
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:3[>=3.16.0][X]
        )
        iasl? ( sys-power/iasl )
        iscsi? ( dev-libs/libiscsi[>=1.9.0] )
        jemalloc? ( dev-libs/jemalloc )
        jpeg? (
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        )
        lzo? ( app-arch/lzo )
        nfs? ( net-fs/libnfs[>=1.9.3] )
        opengl? (
            dev-libs/libepoxy
            x11-dri/mesa[X]
            x11-libs/libX11
        )
        pam? ( sys-libs/pam )
        png? ( media-libs/libpng:= )
        pulseaudio? ( media-sound/pulseaudio )
        rbd? ( sys-cluster/ceph )
        sasl? ( net-libs/cyrus-sasl[>=2.1.22] )
        sdl2? (
            media-libs/SDL:2[>=2.0][X]
            x11-libs/libX11
            sdl-image? ( media-libs/SDL_image:2 )
        )
        smartcard? ( dev-libs/libcacard[>=2.5.1] )
        snappy? ( app-arch/snappy )
        spice? (
            virtualization-lib/spice[>=0.12.0][smartcard?]
            virtualization-lib/spice-protocol[>=0.12.4]
        )
        usb-passthrough? ( dev-libs/libusb:1[>=1.0.13] )
        usb-redirection? ( dev-libs/usbredir )
        vde? ( net/vde[>=2.2.2] )
        virtio-gpu? ( x11-libs/virglrenderer )
        xattr? ( sys-apps/attr )
        xfs? ( sys-fs/xfsprogs )
        !app-emulation/qemu [[
            description = [ Same package, different category ]
            resolution = [ uninstall-blocked-after ]
        ]]
        !app-virtualization/qemu-kvm [[
            description = [ qemu-kvm and qemu finally completely merged again. ]
            resolution = [ uninstall-blocked-after ]
        ]]
        group/kvm
    test:
        dev-libs/libtasn1
        sys-power/iasl
"

if ever at_least 4.2.0; then
    DEPENDENCIES+="
        build+run:
            dev-libs/glib:2[>=2.48]
            sys-libs/libseccomp[>=2.3.0]
            x11-libs/libxkbcommon
            ssh-block-dev? ( net-libs/libssh[>=0.8] )
    "
else
    DEPENDENCIES+="
        build+run:
            ssh-block-dev? ( net-libs/libssh2[>=1.2.8] )

    "
fi

# Most of the intrinsic tests are actually passing now but some are still simply
# broken whereas full-scale testing would entail a lot of effort.
RESTRICT="test"
# If anyone cares enough about the effort:
# https://github.com/avocado-framework/avocado-vt
# http://wiki.qemu.org/Testing
# http://wiki.qemu.org/Testing/LTP
# Last checked: 2.5.0

DEFAULT_SRC_COMPILE_PARAMS=( V=1 )

qemu_src_prepare() {
    # Fix a broken test.
    edo sed -i -e "s:stringify(CONFIG_IASL):\"/usr/$(exhost --build)/bin/iasl\":" tests/bios-tables-test.c

    # Does not work using `cpp` because of invalid options:
    # x86_64-pc-linux-gnu-gcc-cpp: fatal error: '-c' is not a valid option to the preprocessor
    export CPP="${CC} -E"

    default
}

qemu_src_configure() {
    local my_targets=

    local myconfig=(
        --cc=${CC}
        --prefix=/usr
        --bindir=/usr/$(exhost --target)/bin
        --includedir=/usr/$(exhost --target)/include
        --libdir=/usr/$(exhost --target)/lib
        --libexecdir=/usr/$(exhost --target)/libexec
        --docdir=/usr/share/doc/${PNVR}
        --localstatedir=/var
        --python=/usr/$(exhost --build)/bin/python
        --sysconfdir=/etc
        --enable-bzip2
        --enable-cap-ng
        --enable-curl
        --enable-curses
        --enable-fdt
        --enable-guest-agent
        --enable-kvm
        --enable-libxml2
        --enable-linux-user
        --enable-membarrier
        --enable-modules
        # Either nettle *or* gcrypt can be used. I opted for nettle but it could
        # be made into an option if there are *compelling* reasons.
        --enable-nettle
        --enable-pie
        --enable-seccomp
        --enable-stack-protector
        --enable-system
        --enable-tcg-interpreter
        --enable-tpm
        --enable-user
        --enable-vhost-crypto
        --enable-vhost-net
        --enable-vhost-scsi
        --enable-vhost-vsock
        --disable-bochs
        --disable-brlapi
        --disable-cloop
        --disable-debug-mutex
        --disable-dmg
        # Either nettle *or* gcrypt can be used. I opted for nettle but it could
        # be made into an option if there are *compelling* reasons.
        --disable-gcrypt
        --disable-glusterfs
        --disable-libpmem
        --disable-numa
        --disable-parallels
        --disable-pvrdma
        --disable-rdma
        --disable-qed
        --disable-qcow1
        --disable-sheepdog
        --disable-sparse
        --disable-strip
        # Either glibc's malloc, tcmalloc *or* jemalloc can be used.
        # Benchmark-wise: glibc < tcmalloc < jemalloc
        # Could be made into an option if there are *compelling* reasons.
        --disable-tcmalloc
        --disable-vdi
        --disable-vvfat
        --disable-werror
        --disable-xen
        --disable-xen-pci-passthrough
        --without-vss-sdk
        --without-win-sdk
        $(option iasl && echo --iasl=/usr/$(exhost --target)/bin/iasl)
        $(option lzo && echo --enable-lzo)
        $(option_enable async linux-aio)
        $(option_enable bluetooth bluez)
        $(option_enable gnutls)
        $(option_enable iscsi libiscsi)
        $(option_enable jemalloc)
        $(option_enable jpeg vnc-jpeg)
        $(option_enable lzo)
        $(option_enable nfs libnfs)
        $(option_enable opengl)
        $(option_enable png vnc-png)
        $(option_enable rbd)
        $(option_enable sasl vnc-sasl)
        $(option_enable smartcard)
        $(option_enable snappy)
        $(option_enable spice)
        $(option_enable usb-passthrough libusb)
        $(option_enable usb-redirection usb-redir)
        $(option_enable vde)
        $(option_enable virtfs)
        $(option_enable virtio-gpu virglrenderer)
        $(option_enable xattr attr)
        $(option_enable xfs xfsctl)
    )

    if ever at_least 4.2.0; then
        myconfig+=(
            --enable-iconv
            --enable-vhost-kernel
            --enable-vhost-user
            --enable-plugins
            --enable-xkbcommon
            --disable-lzfse
            $(option_enable pam auth-pam)
            $(option_enable ssh-block-dev libssh)
            $(option_enable sdl-image)
        )
    else
        myconfig+=(
            $(option_enable ssh-block-dev libssh2)
        )
        if option sdl2; then
            myconfig+=( --with-sdlabi=2.0 )
        fi
    fi

    # audio drivers
    local myaudiodrv="--audio-drv-list="
    option alsa && myaudiodrv+="alsa "
    option pulseaudio && myaudiodrv+="pa "

    # features
    if option debug ; then
        myconfig+=( --enable-debug-info )
    else
        myconfig+=( --disable-debug-info )
    fi

    if option gtk3 ; then
        myconfig+=(
            --enable-gtk
            --enable-vte
        )
    else
        myconfig+=( --disable-gtk )
    fi

    if option sdl2 ; then
        myconfig+=( --enable-sdl )
        myaudiodrv+="sdl "
    else
        myconfig+=( --disable-sdl )
    fi

    # targets
    option aarch64 && my_targets+=",aarch64-linux-user,aarch64_be-linux-user,aarch64-softmmu"
    option arm && my_targets+=",arm-linux-user,armeb-linux-user,arm-softmmu"
    option amd64 && my_targets+=",x86_64-linux-user,x86_64-softmmu"
    option mips && my_targets+=",mips-linux-user,mips-softmmu"
    option mipsel && my_targets+=",mipsel-linux-user,mipsel-softmmu"
    option ppc && my_targets+=",ppc-linux-user,ppc-softmmu"
    option ppc64 && my_targets+=",ppc64-linux-user,ppc64abi32-linux-user,ppc64le-linux-user,ppc64-softmmu"
    option x86 && my_targets+=",i386-linux-user,i386-softmmu"

    edo ./configure \
        "${myconfig[@]}" \
        "${myaudiodrv}" \
        --target-list=${my_targets:1}
}

qemu_src_test() {
    # Disable the entire network sandboxing...
    esandbox disable_net

    # ... because this...
    esandbox allow_net --connect "unix:/tmp/q*"

    # ... should be enough but actually doesn't fix...
    #bind(-1, unix:/tmp/qtest-4482.sock)
    #connect(-1, unix:/tmp/qgatest.QQvoRj/sock)

    emake check
    esandbox disallow_net --connect "unix:/tmp/q*"
    esandbox enable_net
}

qemu_src_install() {
    default

    insinto /etc/qemu/
    insopts -m0755
    doins "${FILES}"/qemu-if{down,up}
    option sasl && doins qemu.sasl

    newdoc pc-bios/README README.pc-bios
    dodoc -r docs/*
    # dodoc -r QMP

    arch=${CHOST%%-*}
    case ${arch} in
        i486|i586|i686|x86) arch=i386 ;;
        amd64) arch=x86_64 ;;
    esac

    dodir /usr/$(exhost --target)/bin
    dosym /usr/$(exhost --target)/bin/qemu-system-${arch} /usr/$(exhost --target)/bin/qemu
    dosym /usr/$(exhost --target)/bin/qemu-system-${arch} /usr/$(exhost --target)/bin/qemu-kvm

    exeinto /usr/$(exhost --target)/bin
    doexe scripts/kvm/*

    insinto "${UDEVRULESDIR}"
    insopts -m0644
    hereins 65-kvm.rules <<EOF
KERNEL=="kvm", GROUP="kvm", MODE="0660"
EOF
}

qemu_pkg_postinst() {
    local cruft=( /etc/udev/rules.d/65-kvm.rules )
    for file in ${cruft[@]}; do
        if test -f "${file}" ; then
            nonfatal edo rm "${file}" || eerror "removing ${file} failed"
        fi
    done

    elog "Make sure your user is in the 'kvm' group"
    elog
    elog "You will need the Universal TUN/TAP driver compiled into your"
    elog "kernel or loaded as a module to use the virtual network device"
    elog "if using -net tap. You will also need support for 802.1d"
    elog "Ethernet Bridging and a configured bridge if using the provided"
    elog "qemu-ifup script from /etc/qemu."
}

